#!/usr/bin/python3

#   Autor: ghiles KHEDDACHE
#   Data:  28/04/2021


from pyzabbix import ZabbixAPI
from requests import get
import json
import re

zabbix_url      = 'http://yoururl/zabbix'
zabbix_username = 'youruser'
zabbix_password = 'yourpassword'


#'''Connexion a l api Zabbix'''
try:
    zapi = ZabbixAPI(zabbix_url, timeout=4)
    zapi.login(zabbix_username, zabbix_password)
except Exception as err:
     print ('Erreur de connexion a l API Zabbix')   
     print('Erreur: {}'.format(err))


#'''id du hostgroup filter => Discovered hosts'''
dhg = zapi.hostgroup.get(filter={"name": "Discovered hosts"}, output="groupid")

dhg_id = (dhg[0]["groupid"])


"""
Zabbix API PYTHON
More information: https://github.com/lukecyca/pyzabbix
"""


sil = zapi.hostgroup.get(filter={"name":"Site/SIL"}, output=["name","groupid"])
sir = zapi.hostgroup.get(filter={"name":"Site/SIR"}, output=["name","groupid"])
sivm = zapi.hostgroup.get(filter={"name":"Site/SIVM"}, output=["name","groupid"])
inconnu = zapi.hostgroup.get(filter={"name":"Site/Inconnu"}, output=["name","groupid"])



#'''Identification du site en fonction de la premiere lettre du hostname'''
site = {
    "s": sil[0]["groupid"],
    "r": sir[0]["groupid"],
    "b": sivm[0]["groupid"]
}

def getsite(site_id):
  return site.get(site_id, inconnu[0]["groupid"])

def tagexiste(mytaglist, env):
    exist = False
    for mytag in mytaglist: 
        if mytag["tag"] == env:
            exist = True
    return exist
    
def update_tag(host_id, liste_tag, myenv):
    result = 0
    if not tagexiste(liste_tag, myenv): 
        nv_tag = liste_tag
        nv_tag.append({u'tag': myenv, u'value': u''})
        result = zapi.host.update({"hostid": host_id, "tags": nv_tag})
    return result  


# regex de qualif et prod
prod_regex = '\w{3}[eE]\w*'
qualif_regex = '\w{3}[pP]\w*'


#''' Recuparation des hosts apartenant au hostgroup Discovered_hosts'''
#zapi.host.get(groupids=hg_id, output=["hostid","name", "groupids"], selectGroups=["groupid","name"], selectTags="extend")
discovered_hosts = zapi.host.get(groupids=dhg_id, output=["hostid","name"], selectGroups=["groupid"], selectTags="extend")

#''' Identification de l'environnement Prod/Qualif en fonction de la 3eme lettre du hostname'''
for host in discovered_hosts: 
    if re.compile(prod_regex).match(host["name"]):
        envtag = u'PROD'
        update_tag(host["hostid"], host["tags"], envtag) 

    elif re.compile(qualif_regex).match(host["name"]): 
        envtag = u'QUALIF'
        update_tag(host["hostid"], host["tags"], envtag) 

    else :
        envtag = u'ENV-ND'
        update_tag(host["hostid"], host["tags"], envtag) 


for host in discovered_hosts:
    sid = getsite(host["name"][0].lower())
    hg_list = host["groups"]
    hg_list.append({u'groupid': sid})
    hg_list.remove({u'groupid': dhg_id})  # delete host from discovered hosts
# update host with the new associate site's group
    gupdate = zapi.host.update({"hostid": host["hostid"], "groups": hg_list})
